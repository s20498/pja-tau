import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

public class StringOperationsAssertJTest {
    @Test
    public void testReverseString() {
        StringOperations so = new StringOperations();
        assertThat(so.reverse("String")).isEqualTo("gnirtS");
    }

    @Test
    public void testNotConcat(){
        StringOperations so = new StringOperations();
        assertThat(so.concat("StringOne", "StringTwo")).isNotEqualTo(" StringOneStringTwo");
    }

    @Test
    public void testNegativeConcatIsNotNull(){
        StringOperations so = new StringOperations();
        assertThat(so.concat("StringOne", "StringTwo")).isNotNull();
    }

    @Test
    public void testIsPalindrome(){
        StringOperations so = new StringOperations();
        assertThat(so.isPalindrome("racecar")).isTrue();
    }

    @Test
    public void testIsNotPalindromeIsAString(){
        StringOperations so = new StringOperations();
        assertThat(so.isPalindrome("notPalindrome")).isNotInstanceOf(String.class);
    }
}
