def roman(value):
    result = ""
    while value > 0:
        if value >= 1000:
            num_ms = value // 1000
            result += "M" * num_ms
            value -= num_ms * 1000
        elif value >= 900:
            result += "CM"
            value -= 900
        elif value >= 500:
            result += "D"
            value -= 500
        elif value >= 400:
            result += "CD"
            value -= 400
        elif value >= 100:
            num_cs = value // 100
            result += "C" * num_cs
            value -= num_cs * 100
        elif value >= 90:
            result += "XC"
            value -= 90
        elif value >= 50:
            result += "L"
            value -= 50
        elif value >= 40:
            result += "XL"
            value -= 40
        elif value >= 10:
            num_xs = value // 10
            result += "X" * num_xs
            value -= num_xs * 10
        elif value >= 9:
            result += "IX"
            value -= 9
        elif value >= 5:
            result += "V"
            value -= 5
        elif value >= 4:
            result += "IV"
            value -= 4
        else:
            result += "I" * value
            value = 0
    return result


if __name__ == '__main__':
    print(roman(int(input("Enter an integer to be converted to roman: "))))
