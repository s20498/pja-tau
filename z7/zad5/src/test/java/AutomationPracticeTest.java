import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.safari.SafariDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AutomationPracticeTest {

    private static ChromeDriver driver;
    private static SafariDriver safariDriver;

    @BeforeAll
    public static void setUpDriver(){
        WebDriverManager.chromedriver().setup();
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        safariDriver = new SafariDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        safariDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @BeforeEach
    public void setUp() throws Exception {
        safariDriver.get("http://automationpractice.pl/index.php");
        driver.get("http://automationpractice.pl/index.php");
    }

    @Test
    public void testWillFindBlouse() {
        WebElement searchBar = driver.findElement(By.id("search_query_top"));
        searchBar.sendKeys("blouse");
        driver.findElement(By.name("submit_search")).click();
        assertEquals(true, driver.getPageSource().contains("1 result has been found."));
    }

    @Test
    public void testSafariWillFindBlouse() throws InterruptedException {
        WebElement searchBar =safariDriver.findElement(By.id("search_query_top"));
        searchBar.sendKeys("Blouse");
        WebElement searchButton = safariDriver.findElement(By.cssSelector(".button-search"));
        searchButton.click();
        assertEquals(true, safariDriver.getPageSource().contains("1 result has been found."));
    }

    @Test
    public void testContactUsPage() {
        WebElement contactUsButton = driver.findElement(By.id("contact-link"));
        contactUsButton.click();
        assertEquals("Contact us - My Store", driver.getTitle());
    }

    @Test
    public void testSafariContactUsPage() {
        WebElement contactUsButton = safariDriver.findElement(By.id("contact-link"));
        contactUsButton.click();
        assertEquals("Contact us - My Store", safariDriver.getTitle());
    }

    @Test
    public void testSearchBackToHome() {
        WebElement searchBar = driver.findElement(By.id("search_query_top"));
        searchBar.sendKeys("blouse");
        driver.findElement(By.name("submit_search")).click();
        driver.navigate().back();
        assertEquals("My Store", driver.getTitle());
    }

    @Test
    public void testSafariSearchBackToHome() {
        WebElement searchBar = safariDriver.findElement(By.id("search_query_top"));
        searchBar.sendKeys("blouse");
        safariDriver.findElement(By.name("submit_search")).click();
        safariDriver.navigate().back();
        assertEquals("My Store", safariDriver.getTitle());
    }


}
