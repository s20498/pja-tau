import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringOperationsTest {

    @Test
    public void testReverseString() {
        StringOperations so = new StringOperations();
        assertEquals(so.reverse("String"), "gnirtS");
    }

    @Test
    public void testWouldNotReverseInteger() {
        StringOperations so = new StringOperations();
        so.reverse("1");
        Throwable exception = assertThrows(Exception.class, () -> {
            throw new IllegalArgumentException("invalid type");
        });
        assertEquals("invalid type", exception.getMessage());
    }

    @Test
    public void testConcat(){
        StringOperations so = new StringOperations();
        assertEquals(so.concat("StringOne", "StringTwo"), "StringOneStringTwo");
    }

    @Test
    public void testNegativeConcat(){
        StringOperations so = new StringOperations();
        assertNotEquals(so.concat("StringOne", "StringTwo"), "StringOneTwoString");
    }

    @Test
    public void testIsPalindrome(){
        StringOperations so = new StringOperations();
        assertEquals(so.isPalindrome("racecar"), true);
    }

    @Test
    public void testIsNotPalindrome(){
        StringOperations so = new StringOperations();
        assertNotEquals(so.isPalindrome("notPalindrome"), true);
    }


}