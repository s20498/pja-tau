import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class WebsiteXPathTest {

    private static WebDriver driver;

    @BeforeAll
    public static void setUpDriver(){
        WebDriverManager.chromedriver().setup();
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @BeforeEach
    public void setUp() throws Exception {
        driver.get("https://duckduckgo.com");
    }

    @Test
    public void testCountNumberOfResultsGreaterThan10() {
        driver.get("https://duckduckgo.com/?q=selenium");
        List<WebElement> links = driver.findElements(By.xpath("//a"));
        assertThat(links.size(), greaterThan(10));
    }

    @Test
    public void testCountNumberOfImagesGreaterThan10() {
        driver.get("https://duckduckgo.com/?q=selenium");
        List<WebElement> links = driver.findElements(By.xpath("//img"));
        assertThat(links.size(), greaterThan(10));
    }

    @Test
    public void testAllLinksAreClickable() {
        driver.get("https://duckduckgo.com/?q=selenium");
        List<WebElement> links = driver.findElements(By.xpath("//article"));

        for (WebElement link : links) {
            if (!link.getText().isEmpty()) {
                System.out.println(link);
                link.click();
                driver.navigate().back();
            }

        }

        assertEquals(10, links.size());
    }

    @Test
    public void testAllTextFieldsNotLessThanTwo() {
        driver.get("http://automationpractice.pl/index.php?controller=contact");
        List<WebElement> textFields = driver.findElements(By.xpath("//textarea"));

        assertEquals(1, textFields.size());
    }

}
