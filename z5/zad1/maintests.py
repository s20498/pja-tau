import json
import unittest

from hamcrest import starts_with, assert_that, not_none, raises, calling, ends_with

import main


class MyTestCase(unittest.TestCase):

    def test_statement_has_company_name(self):
        assert_that(main.run_statement('plays.json', 'file', 'invoice.json', 'file'), starts_with("Statement for"))

    def test_correct_volume_credits_value(self):
        invoice_data = """
        {
          "customer": "BigCo",
          "performances": [
            {
              "playID": "hamlet",
              "audience": 55
            },
            {
              "playID": "as-like",
              "audience": 35
            },
            {
              "playID": "othello",
              "audience": 40
            }
          ]
        }
        """

        plays_data = """
        {
            "hamlet": {"name": "Hamlet", "type": "tragedy"},
            "as-like": {"name": "As You Like It", "type": "comedy"},
            "othello": {"name": "Othello", "type": "comedy"}
        }
        """

        assert_that(main.run_statement(plays_data, 'raw', invoice_data, 'raw'), ends_with("55 credits\n"))

    def test_will_detect_unknown_play(self):
        invoice_data = """
        {
          "customer": "BigCo",
          "performances": [
            {
              "playID": "hamlet",
              "audience": 55
            },
            {
              "playID": "as-like",
              "audience": 35
            },
            {
              "playID": "othello",
              "audience": 40
            }
          ]
        }
        """

        plays_data = """
        {
            "hamlet": {"name": "Hamlet", "type": "tragedy"},
            "as-like": {"name": "As You Like It", "type": "comedy"},
            "othello": {"name": "Othello", "type": "tragicomedy"}
        }
        """

        assert_that(calling(main.run_statement).with_args(plays_data, 'raw', invoice_data, 'raw'), raises(ValueError))



if __name__ == '__main__':
    unittest.main()
