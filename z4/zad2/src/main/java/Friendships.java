import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Friendships {
    Map<String, List<String>> friendships = new HashMap<>();

    //Dodanie przyjaciół - wykorzystuje funkcje addFriend!
    public void makeFriends(String person1, String person2) {
        addFriend(person1, person2);
        addFriend(person2, person1);
    }

    //Pobranie listy przyjaciol
    public List<String> getFriendsList(String person) {
        return friendships.get(person);
    }

    //Sprawdzenie czy przyjaciele
    public boolean areFriends(String person1, String person2) {
        List<String> person1sFriends = getFriendsList(person1);
        List<String> person2sFriends = getFriendsList(person2);
        return person1sFriends.retainAll(person2sFriends);
    }
    //Dodanie do listy przyjaciol do danej osoby
    private void addFriend(String person, String friend) {
        List<String> friends;

        if (friendships.get(person) == null) {
            friends = new ArrayList<>();
        } else {
            friends = getFriendsList(person);
        }

        friends.add(friend);
        friendships.put(person, friends);
    }
}