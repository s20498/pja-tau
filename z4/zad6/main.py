class Car():
    
    def __init__(self, __year_model, __make):
        self.__year_model = __year_model
        self.__make = __make
        self.__speed = 0

    def accelerate(self):
        self.__speed += 5

    def brake(self):
        if (self.__speed != 0):
            self.__speed -= 5

    def get_speed(self):
        return self.__speed

    def get_make(self):
        return self.__make

    def get_model(self):
        return self.__year_model

import unittest
from assertpy import assert_that

class CarTest(unittest.TestCase):
    def test_car_accelerates(self):
        car = Car("2020 Clio", "Renault")
        car.accelerate()
        assert_that(car.get_speed()).is_equal_to(5)

    def test_car_brakes_with_no_speed(self):
        car = Car("2020 Clio", "Renault")
        car.brake()

        assert_that(car.get_speed()).is_not_equal_to(5)

    def test_car_brakes_twice_with_no_speed(self):
        car = Car("2020 Clio", "Renault")
        car.brake()
        car.brake()

        assert_that(car.get_speed()).is_instance_of(int)

    def test_car_may_be_Renault(self):
        car = Car("2020 Clio", "Renault")

        assert_that(car.get_make()).starts_with("R")

    def test_car_is_clio(self):
        car = Car("2020 Clio", "Renault")

        assert_that(car.get_model()).contains("Clio")

    def test_car_is_not_polonez(self):
        car = Car("2020 Clio", "Renault")

        assert_that(car.get_model()).does_not_contain("Polonez")


    def test_car_has_no_negative_speed_at_start(self):
        car = Car("2020 Clio", "Renault")

        assert_that(car.get_speed()).is_greater_than_or_equal_to(0)

    
    def test_car_exists(self):
        car = Car("2020 Clio", "Renault")

        assert_that(car).is_not_none()

if __name__ == '__main__':
    unittest.main()