package pl.pjatk.model;

public class Note {
    private Integer note;
    private String name;

    public Note(Integer note, String name) {
        this.note = note;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getNote() {
        return note;
    }
}
