def distance(first, second):
    first_len = len(first)
    second_len = len(second)
    validate(first_len, second_len)

    if (first_len == 0 and second_len == 0) or (first == second):
        return 0
    elif (first_len == 1 and second_len == 1) and (first != second):
        return 1
    else:
        return 9


def validate(first, second):
    if first != second:
        raise ValueError("Invalid comparison")


if __name__ == '__main__':
    print(distance(input("Enter first Hamming string: "), input("Enter second Hamming string: ")))