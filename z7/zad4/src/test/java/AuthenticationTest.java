import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuthenticationTest {

    private static WebDriver driver;

    @BeforeAll
    public static void setUpDriver(){
        WebDriverManager.chromedriver().setup();
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
    }

    @BeforeEach
    public void setUp() throws Exception {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://automationpractice.pl/index.php?controller=authentication");
    }

    @Test
    public void testIncorrectCredentialsLoginFailed() {
        WebElement username = driver.findElement(By.id("email"));
        WebElement password = driver.findElement(By.id("passwd"));
        username.sendKeys("test1@test.com");
        username.sendKeys(Keys.TAB);
        password.sendKeys("test1231");
        password.sendKeys(Keys.TAB);
        driver.findElement(By.id("SubmitLogin")).click();
        assertEquals("Login - My Store", driver.getTitle());
    }

    @Test
    public void testEmailEmptyLoginFailed() {
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("test1231");
        password.sendKeys(Keys.TAB);
        driver.findElement(By.id("SubmitLogin")).click();
        assertEquals("Login - My Store", driver.getTitle());
    }

    @Test
    public void testPasswordEmptyLoginFailed() {
        WebElement username = driver.findElement(By.id("email"));
        username.sendKeys("test1@test.com");
        username.sendKeys(Keys.TAB);
        driver.findElement(By.id("SubmitLogin")).click();
        assertEquals("Login - My Store", driver.getTitle());
    }

    @Test
    public void testLoggedIn() {
        WebElement username = driver.findElement(By.id("email"));
        WebElement password = driver.findElement(By.id("passwd"));
        username.sendKeys("test1@test.com");
        username.sendKeys(Keys.TAB);
        password.sendKeys("test123");
        password.sendKeys(Keys.TAB);
        driver.findElement(By.id("SubmitLogin")).click();
        assertEquals("My account - My Store", driver.getTitle());
    }

}
