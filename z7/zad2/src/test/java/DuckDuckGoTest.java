import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

public class DuckDuckGoTest {

    private static WebDriver driver;

    @BeforeAll
    public static void setUpDriver(){
        WebDriverManager.chromedriver().setup();
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @BeforeEach
    public void setUp() throws Exception {
        driver.get("https://duckduckgo.com");
    }

    // Jak wejść w pierwszy i trzeci otrzymany wynik?
    @Test
    public void testPressFirstAndThirdResultWillNavigateBack() {
        driver.findElement(By.name("q")).sendKeys("selenium");
        driver.findElement(By.cssSelector("button[aria-label='Search")).click();
        driver.findElement(By.id("r1-0")).click();
        driver.navigate().back();
        driver.findElements(By.tagName("article")).get(2).click();
        driver.navigate().back();
        assertEquals("selenium at DuckDuckGo", driver.getTitle());
    }

    // Czy istnieje inna metoda na kliknięcie niż click()?
    @Test
    public void testWillSearchBoxSubmitForm() {
        driver.findElement(By.id("searchbox_input")).sendKeys("selenium");
        driver.findElement(By.id("searchbox_input")).submit();
        assertEquals("selenium at DuckDuckGo", driver.getTitle());
    }

    // Co w przypadku kiedy nie znajdziemy szukanego elementu, który wyszukujemy na stronie?
    @Test
    public void testInvalidElementWillThrowException() {
        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> driver.findElement(By.tagName("invalid")));
    }

    @Test
    public void testTitlePage() {
        assertEquals("DuckDuckGo — Privacy, simplified.", driver.getTitle());
    }

}
