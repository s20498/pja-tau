package pl.pjatk.storage;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import pl.pjatk.model.Note;

import java.util.List;

public class NotesStorageMock implements NotesStorage {
    Multimap<String, Note> notesByName = ArrayListMultimap.create();
    @Override
    public void add(Note note) {
        notesByName.put(note.getName(), note);
    }

    @Override
    public List<Note> getAllNotesOf(String name) {
        return (List<Note>) notesByName.get(name);
    }

    public Integer getSize() {
        return notesByName.size();
    }

    @Override
    public void clear() {
        notesByName.clear();
    }
}
