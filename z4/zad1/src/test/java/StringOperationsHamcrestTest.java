import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import static org.junit.jupiter.api.Assertions.*;

public class StringOperationsHamcrestTest {

    @Test
    public void testReverseString() {
        StringOperations so = new StringOperations();
        assertThat(so.reverse("String"), equalTo("gnirtS"));
    }

    @Test
    public void testNotConcat(){
        StringOperations so = new StringOperations();
        assertThat(so.concat("StringOne", "StringTwo"), not(" StringOneStringTwo"));
    }

    @Test
    public void testNegativeConcatIsNotNull(){
        StringOperations so = new StringOperations();
        assertThat(so.concat("StringOne", "StringTwo"), notNullValue());
    }

    @Test
    public void testIsPalindrome(){
        StringOperations so = new StringOperations();
        assertThat(so.isPalindrome("racecar"), is(true));
    }

    @Test
    public void testIsNotPalindromeIsAString(){
        StringOperations so = new StringOperations();
        assertThat(so.isPalindrome("notPalindrome"), isA(String.class));
    }


}