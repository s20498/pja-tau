import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class FriendshipsHamcrestTests {

    @Test
    public void testHasNoFriendships() {
        Friendships friendships = new Friendships();
        String person1 = "adam";
        List<String> friends = friendships.getFriendsList(person1);
        assertThat(friends, nullValue());
    }

    @Test
    public void testHasOneFriendship() {
        Friendships friendships = new Friendships();
        String person1 = "adam";
        String person2 = "tom";
        friendships.makeFriends(person1, person2);
        assertThat(friendships.getFriendsList(person1), is(List.of(person2)));
    }

    @Test
    public void testAreBothFriends() {
        Friendships friendships = new Friendships();
        String person1 = "adam";
        String person2 = "tom";
        friendships.makeFriends(person1, person2);
        assertThat(friendships.areFriends(person1, person2), is(true));
    }

    @Test
    public void testWillNotMakeDuplicateFriends() {
        Friendships friendships = new Friendships();
        String person1 = "adam";
        String person2 = "tom";
        friendships.makeFriends(person1, person2);
        friendships.makeFriends(person1, person2);
        assertThat(friendships.areFriends(person1, person2), is(true));
    }

    @Test
    public void testHasInvalidFriend() {
        Friendships friendships = new Friendships();
        String person1 = "adam";
        String person2 = "tom";
        friendships.makeFriends(person1, person2);
        assertThat(friendships.getFriendsList(person1), not(List.of("mot")));
    }

    @Test
    public void testFriendsListHasProperSize() {
        Friendships friendships = new Friendships();
        String person1 = "adam";
        String person2 = "tom";
        friendships.makeFriends(person1, person2);
        assertThat(friendships.getFriendsList(person1), hasSize(1));
    }

    @Test
    public void testFriendshipsIsProperType() {
        Friendships friendships = new Friendships();
        assertThat(friendships, instanceOf(Friendships.class));
    }
}
