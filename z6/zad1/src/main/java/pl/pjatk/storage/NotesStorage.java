package pl.pjatk.storage;

import pl.pjatk.model.Note;

import java.util.List;

public interface NotesStorage {
    void add(Note note);
    List<Note> getAllNotesOf(String name);
    void clear();
}
