package pl.pjatk;

import pl.pjatk.model.Note;
import pl.pjatk.service.NotesService;
import pl.pjatk.storage.NotesStorage;

import java.util.Collection;

// Pomyliłem się i miałem tutaj wstawić link do testów :(
// Znajdziesz je tutaj: https://gitlab.com/s20498/pja-tau/-/blob/master/z6/zad1/src/test/java/pl/pjatk/NotesServiceImplTest.java

public class NotesServiceImpl implements NotesService {

    public static NotesServiceImpl createWith(final NotesStorage storageService) {
        return new NotesServiceImpl(storageService);
    }

    @Override
    public void add(Note note) {
        storageService.add(note);
    }

    @Override
    public float averageOf(String name) {
        float sum = 0.0f;
        final Collection<Note> notes = storageService.getAllNotesOf(name);
        for (final Note note : notes) {
            sum += note.getNote();
        }
        return sum / notes.size();
    }

    @Override
    public void clear() {
        storageService.clear();
    }

    private NotesServiceImpl(final NotesStorage storageService) {
        this.storageService = storageService;
    }

    private final NotesStorage storageService;
}