package pl.pjatk;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.pjatk.model.Note;
import pl.pjatk.storage.NotesStorageMock;

import static org.junit.jupiter.api.Assertions.*;

class NotesServiceImplTest {


    NotesStorageMock notesStorageMock;
    NotesServiceImpl notesService;

    @BeforeEach
    public void setUp() {
        notesStorageMock = new NotesStorageMock();
        notesService = NotesServiceImpl.createWith(notesStorageMock);
        notesService.add(new Note(1, "personOne"));
        notesService.add(new Note(3, "personOne"));
        notesService.add(new Note(4, "personTwo"));
    }

    @Test
    void average_of_person_one_returns_2() {
        // when
        float avg = notesService.averageOf("personOne");

        // then
        assertEquals(2.0f, avg);
    }

    @Test
    void average_of_person_one_does_not_return_3() {
        // when
        float avg = notesService.averageOf("personOne");

        // then
        assertNotEquals(3.0f, avg);
    }

    @Test
    void average_of_nonexisting_name() {
        // when
        float avg = notesService.averageOf("personThree");

        // then
        assertTrue(Float.isNaN(avg));
    }

    @Test
    void clear() {
        // when
        notesService.clear();

        // then
        assertEquals((notesStorageMock.getSize()), 0);
    }
}