import json
import math


def run_statement(plays, plays_mode, invoices, invoices_mode):

    if invoices_mode == 'file':
        invoice_file = open(invoices)
        invoice_data = json.load(invoice_file)
        invoice_file.close()
    elif invoices_mode == 'raw':
        invoice_data = json.loads(invoices)

    if plays_mode == 'file':
        plays_file = open(plays)
        plays_data = json.load(plays_file)
        plays_file.close()
    elif plays_mode == 'raw':
        plays_data = json.loads(plays)

    return statement(invoice_data, plays_data)


def statement(invoice, plays):
    total_amount = 0
    volume_credits = 0
    result = f'Statement for {invoice["customer"]}\n'

    def format_as_dollars(amount):
        return f"${amount:0,.2f}"

    for perf in invoice['performances']:
        play = plays[perf['playID']]
        if play['type'] == "tragedy":
            this_amount = 40000
            if perf['audience'] > 30:
                this_amount += 1000 * (perf['audience'] - 30)
        elif play['type'] == "comedy":
            this_amount = 30000
            if perf['audience'] > 20:
                this_amount += 10000 + 500 * (perf['audience'] - 20)

            this_amount += 300 * perf['audience']

        else:
            raise ValueError(f'unknown type: {play["type"]}')

        # add volume credits
        volume_credits += max(perf['audience'] - 30, 0)
        # add extra credit for every ten comedy attendees
        if "comedy" == play["type"]:
            volume_credits += math.floor(perf['audience'] / 5)
        # print line for this order
        result += f' {play["name"]}: {format_as_dollars(this_amount/100)} ({perf["audience"]} seats)\n'
        total_amount += this_amount

    result += f'Amount owed is {format_as_dollars(total_amount/100)}\n'
    result += f'You earned {volume_credits} credits\n'
    return result


if __name__ == '__main__':
    print(run_statement('plays.json', 'file', 'invoice.json', 'file'))
