def FizzBuzz(num):
    if (((num % 5) == 0) & ((num % 3) == 0)):
        return "FizzBuzz"
    elif ((num % 5) == 0):
        return "Buzz"
    elif ((num % 3) == 0):
        return "Fizz"
    else:
        raise Exception("Error in FizzBuzz")


from hamcrest import assert_that, equal_to, is_not, has_string, starts_with, string_contains_in_order, calling, raises, has_length, instance_of, equal_to_ignoring_case
import unittest

class FizzBuzzTest(unittest.TestCase):
    def test_Fizz_Buzz_divisible_by_5(self):
        assert_that("Buzz", equal_to(FizzBuzz(5)))
    
    def test_Fizz_Buzz_not_divisible_by_9(self):
        assert_that("Buzz", is_not(FizzBuzz(9.0)))

    def test_Fizz_Buzz_has_string_when_divisible_by_5(self):
        assert_that(FizzBuzz(5), has_string("Buzz"))

    def test_Fizz_Buzz_divisible_by_6(self):
        assert_that(FizzBuzz(6), starts_with("Fizz"))

    def test_Fizz_Buzz_is_both_divisible_by_3_and_5(self):
        assert_that(FizzBuzz(15), string_contains_in_order("Fi", "Bu"))

    def test_Fizz_Buzz_negative_number(self):
        assert_that(FizzBuzz(-3), has_length(4))

    def test_Fizz_Buzz_has_a_string(self):
        assert_that(FizzBuzz(6), instance_of(str))

    def test_Fizz_Buzz_has_proper_text_returned(self):
        assert_that(FizzBuzz(6), equal_to_ignoring_case("fizz"))

if __name__ == '__main__':
    unittest.main()